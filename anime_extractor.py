""" Позволяет получить компоненты по XPath с Web-страницы """

from typing import Any
import json

from lxml import html
import requests


def _get_json(file_name: str) -> dict:
    """ Загружает JSON-объект из файла """
    with open(file_name, 'rb') as file:
        obj = json.load(file)
    return obj


def __get_from_json(json_dict: dict, key: str) -> Any:
    """ Возвращает значение по ключу из словаря JSON-объекта """
    return json_dict[key]


def _get_link(json_dict: dict) -> str:
    """ Возвращает значение URL из JSON """
    return __get_from_json(json_dict, 'Link')


def _get_xpath(json_dict: dict) -> str:
    """ Возвращает значение XPath из JSON """
    return __get_from_json(json_dict, 'XPath')


def _get_page_by_url(url: str) -> str:
    """ Получает web-cтраницу по ссылке """
    request = requests.get(url)
    return request.text


def _get_raw_contents_by_xpath(page: str, xpath: str) -> list[Any]:
    """ Возвращает список вхождений XPath """
    tree = html.fromstring(page)
    raw_contents = tree.xpath(xpath)
    return [content.text for content in raw_contents]


def get_all_content_from_file(filename: str,) -> list[str]:
    json_obj = _get_json(filename)

    url = _get_link(json_obj)
    xpath = _get_xpath(json_obj)

    page = _get_page_by_url(url)
    return _get_raw_contents_by_xpath(page, xpath)


def main():
    """ Главная функция программы """
    filename = 'animes.json'
    content = get_all_content_from_file(filename)
    content = [line[2:] for line in content]
    with open("Anime list.txt", 'w') as file:
        for number, anime in enumerate(content, start=1):
            print(f'{number}) {anime}')
            file.write(f'{number}) {anime}\n')


if __name__ == '__main__':
    main()
